#!/usr/bin/env python
# -*- coding: utf-8 -*-
from argparse import ArgumentParser as ap
import numpy as np
import astropy.io.fits as fits
import os.path as path
from ujigawa.misc import command_line_function

@command_line_function
def main(*argv):
  parser = ap(description='Make a dark-frame from a set of images.')

  parser.add_argument('fits', type=str,
                      help='fits file to compile a dark-frame')
  parser.add_argument('out', type=str, nargs='?',
                      help='output fits file')
  parser.add_argument('-f', dest='overwrite', action='store_true',
                      help='overwrite a fits image')

  args = parser.parse_args(argv[1:])

  hdu = fits.open(args.fits)
  hdr = hdu[0].header

  hdu[0].data = np.median(hdu[0].data, axis=0)
  hdu[0].header.add_history(
    '[makedark] created from {}.'.format(path.basename(args.fits)))

  if args.out is None:
    nx1 = hdr.get('NAXIS1')
    nx2 = hdr.get('NAXIS2')
    qid = hdr.get('QUAD_ID')
    did = hdr.get('DET_ID')
    texp = int(hdr.get('TFRAME')*1000)
    output = 'dark_{}x{}_{:04d}-{:03d}.fits'.format(nx1,nx2,texp,did)
  else:
    output = args.out

  hdu.writeto(output, overwrite=args.overwrite)


if __name__ == '__main__':
  main()
