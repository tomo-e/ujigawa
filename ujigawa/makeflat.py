#!/usr/bin/env python
# -*- coding: utf-8 -*-
from argparse import ArgumentParser as ap
import astropy.io.fits as fits
import numpy as np
import os.path as path
from ujigawa.misc import command_line_function

@command_line_function
def main(*argv):
  parser = ap(description='Make a flat-frame from a set of images.')

  parser.add_argument('flat', type=str,
                      help='a raw flat frame')
  parser.add_argument('dark', type=str, nargs='?',
                      help='a dark frame to compile a flat-frame')
  parser.add_argument('out', type=str, nargs='?',
                      help='output fits file')
  parser.add_argument('-f', dest='overwrite', action='store_true',
                      help='overwrite a fits image')

  args = parser.parse_args(argv[1:])

  hdu = fits.open(args.flat)
  hdr = hdu[0].header
  if hdu[0].data.ndim == 3:
    hdu[0].data = np.median(hdu[0].data, axis=0)

  if args.dark is None:
    nx1 = hdr.get('NAXIS1')
    nx2 = hdr.get('NAXIS2')
    qid = hdr.get('QUAD_ID')
    did = hdr.get('DET_ID')
    texp = int(hdr.get('TFRAME')*1000)
    darkfits = 'dark_{}x{}_{:04d}-{:03d}.fits'.format(nx1,nx2,texp,did)
  else:
    darkfits = args.out

  dark_hdu = fits.open(darkfits)
  if dark_hdu[0].data.ndim == 3:
    dark_hdu[0].data = np.median(dark_hdu[0].data, axis=0)

  hdu[0].data -= dark_hdu[0].data
  hdu[0].data /= np.median(hdu[0].data)
  hdu[0].header.add_history(
    '[makeflat] created from {}.'.format(path.basename(args.flat)))
  hdu[0].header.add_history(
    '[makeflat] created from {}.'.format(path.basename(darkfits)))

  if args.out is None:
    nx1 = hdr.get('NAXIS1')
    nx2 = hdr.get('NAXIS2')
    qid = hdr.get('QUAD_ID')
    did = hdr.get('DET_ID')
    output = 'flat_{}x{}-{:03d}.fits'.format(nx1,nx2,did)
  else:
    output = args.out
  hdu.writeto(output, overwrite=args.overwrite)


if __name__ == '__main__':
  main()
