#!/usr/bin/env python
# -*- coding: utf-8 -*-
from argparse import ArgumentParser as ap
import astropy.io.fits as fits
import os.path as path
from ujigawa.misc import command_line_function


@command_line_function
def main(*argv):
  parser = ap(description='Obtain differential frames along NAXIS3')

  parser.add_argument('fits', help='input fits file')
  parser.add_argument('out', help='output fits file')
  parser.add_argument('-f', dest='overwrite', action='store_true',
                      help='overwrite a fits image')

  args = parser.parse_args(argv[1:])

  hdu = fits.open(args.fits)

  if hdu[0].data.ndim != 3:
    raise ValueError('input fits file should be 3-dimensional')

  z,y,x = hdu[0].data.shape
  data0 = hdu[0].data[slice(0,z-1),:,:]
  data1 = hdu[0].data[slice(1,z),:,:]
  hdu[0].data = data1 - data0
  hdu[0].header.add_history(
    '[imzdiff] created from {}.'.format(path.basename(args.fits)))

  hdu.writeto(args.out, overwrite=args.overwrite)


if __name__ == '__main__':
  main()
