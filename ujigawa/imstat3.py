#!/usr/bin/env python
# -*- coding: utf-8 -*-
from argparse import ArgumentParser as ap
from astropy.time import Time
import astropy.io.fits as fits
import astropy.units as u
import os.path as path
from ujigawa.misc import statistics
from ujigawa.misc import command_line_function


@command_line_function
def main(*argv):
  parser = ap(description='Calculate statistics along with NAXIS3')

  parser.add_argument('method', choices=('mean','median','max','min','std'),
                      help='statistics to be calculated')
  parser.add_argument('fits', help='input fits file')
  parser.add_argument('out', help='output fits file')
  parser.add_argument('-f', dest='overwrite', action='store_true',
                      help='overwrite a fits image')

  args = parser.parse_args()
  method = statistics(args.method)

  hdu = fits.open(args.fits)
  data = hdu[0].data.view()

  if data.ndim != 3:
    raise RuntimeError('input fits file is not 3-dimensional')

  z  = int(hdu[0].header.get('NAXIS3',1))
  dT  = float(hdu[0].header.get('TFRAME', 0.0))
  crpix3 = float(hdu[0].header.get('CRPIX3', 0.0))
  hdu[0].header.set('TFRAME', z*dT)
  hdu[0].header.set('CRPIX3', crpix3/z)

  hdu[0].data = method(data, axis=0)
  hdu[0].header.add_history(
    '[imstat3] statistics: {}.'.format(args.method))
  hdu[0].header.add_history(
    '[imstat3] created from {}.'.format(path.basename(args.fits)))

  hdu.writeto(args.out, overwrite=args.overwrite)


if __name__ == '__main__':
  main()
