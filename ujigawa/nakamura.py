#!/usr/bin/env python
# -*- coding: utf-8 -*-
from argparse import ArgumentParser as ap
from sklearn.utils.extmath import randomized_svd
from skimage.morphology import disk, dilation, binary_dilation
from skimage.measure import label
from numba import jit,int16,float32
from astropy.time import Time
import numpy as np
import astropy.io.fits as fits
import warnings, sys
from ujigawa.misc import command_line_function


@jit(float32[:,:,:](float32[:,:,:],int16))
def squish_image(image, shrink):
  ''' Squish the size of array

  Return a shrinked image array or cube. The x- and y-axes are shrinked by
  a factor of `shrink`.

  Args:
    image: ndarray containing a 2d- or 3d-image
    shrink: squishing factor (should be a positive integer)
  '''
  z,y,x = image.shape
  ny,nx = y//shrink, x//shrink
  image = image[:,:ny*shrink,:nx*shrink]
  new_image = np.zeros((z,ny,nx), dtype=np.float32)
  for my in range(shrink):
    for mx in range(shrink):
      new_image += image[:,my::shrink,mx::shrink]
  return new_image


def full_rank(cube):
  ''' Convert a given cube into a full rank

  Args:
    cube: an ndarray, containing multiple frames
  '''
  z,y,x = cube.shape
  array = cube.reshape((z,y*x))
  u,s,v = np.linalg.svd(array)
  K = np.sum(s>0)
  tmp = np.matmul(np.matmul(u[:K,:K],np.diag(s[:K])),v[:K,:])
  return tmp.reshape(K,y,x)


def nakamura_approx(array, refs, const=0.0):
  ''' Approximate matrix using the floating Nakamura-method

  Args:
    array: an ndarray, containing a target frame
    refs: an ndarray, which contains reference frames
  '''
  z,y,x = refs.shape
  t = array.reshape((1,y*x)).astype(np.float64)
  r = refs.reshape((z,y*x)).astype(np.float64)
  t1,r1 = np.sum(t, axis=1), np.sum(r, axis=1).reshape((z,1))
  rt,rr = np.sum(t*r, axis=1).reshape((z,1)), np.matmul(r,r.T)
  A = z*rr - r1*r1.T - const
  b = z*rt - r1*t1
  alpha = (np.linalg.solve(A,b)).reshape((z,1,1))
  beta = (t1 - np.sum(refs*alpha))/(y*x)

  return (np.sum(refs*alpha, axis=0)+beta).astype(np.float32)


def hard_threshold(array, reference, threshold):
  ''' Compute a masking matrix by the hard-thresholding method

  Extract bright pixels, which are brighter than the threshold in both
  the target and reference images.

  Args:
    array: an ndarray to be converted
    reference: an ndarray to identify pixels to be fixed
    threshold: an threshold level
  '''
  absolute = np.abs(array)
  absolute[(absolute<threshold) | (reference<threshold)] = 0.0
  return np.sign(array)*absolute


def subtract_with_references(array, refs, sigma=3.0, max_iter=3):
  ''' Subtract background using the Nakamura-method

  Args:
    array: an ndarray, which contains a target frame
    refs: an ndarray, which contains reference frames
    sigma: a threshold for bright pixel masking (optional)
    max_iter: maximum iteration for sigma-clipping to obtain stddev (optional)
  '''
  y,x = array.shape
  norm = np.median(array)
  array, refs = array/norm, refs/norm
  median = np.median(refs, axis=0)
  approx = np.zeros_like(array, dtype=np.float32)
  sparse = np.zeros_like(array, dtype=np.float32)

  approx = nakamura_approx(array, refs, const=0.0)
  stddev = np.std(array-approx)
  #print('stddev    = {} ({})'.format(stddev*norm,
  #        np.std(array-np.mean(refs,axis=0))*norm), file=sys.stderr)
  for n in range(max_iter):
    flag = np.abs(array-approx) < sigma*stddev
    stddev = np.std((array-approx)[flag])
  masked = hard_threshold(array-approx, median-1.0, threshold=sigma*stddev)
  #print('threshold = {}'.format(sigma*stddev*norm), file=sys.stderr)
  print('residual = {} ({})'.format(np.mean(array-approx-masked)*norm,
                                     np.std(array-approx-masked)*norm),
        file=sys.stderr)
  return (array - approx - masked)*norm, (array - np.median(array))*norm


@command_line_function
def main(*argv):
  parser = ap(
    description='subtract background using Nakamura-method')

  parser.add_argument('fits', help='input fits image')
  parser.add_argument('frame', type=int, help='fits frame number')
  parser.add_argument('output', help='output fits file name')

  parser.add_argument('--shrink', action='store', type=int, default=4,
                      help='image squishing scale [default=4]')
  parser.add_argument('--sigma', action='store', type=float, default=3.0,
                      help='threshold of soft-threshold [default=3.0]')
  parser.add_argument('--max_iter', action='store', type=int, default=30,
                      help='maximum number of iterations [default=30]')

  parser.add_argument('-v', dest='verbose', action='store_true',
                      help='enable verbose output')
  parser.add_argument('-f', dest='overwrite', action='store_true',
                      help='overwrite the output fits file if exists')

  args = parser.parse_args(argv[1:])

  hdu = fits.open(args.fits)

  image = hdu[0].data
  image = squish_image(image, shrink=args.shrink)
  if image.ndim != 3:
    raise RuntimeError('fits file should contain a 3d-image')

  z,y,x = image.shape
  n = args.frame - 1

  if z <= 1:
    raise RuntimeError('fits file should contain more than one frame')

  frame_list = range(z)
  target = image[n]
  reference_list = np.setxor1d(frame_list, n)
  references = image[reference_list]

  subtracted = subtract_with_references(
    target, references, sigma=args.sigma, max_iter=args.max_iter)

  src = hdu[0].header
  new = fits.PrimaryHDU()
  new.data = subtracted

  header_keys = (
    'OBJECT', 'PROJECT', 'OBSERVER',
    'EXPTIME', 'TELAPSE', 'EXPTIME1', 'TFRAME', 'DATA-FPS',
    'DATE-OBS', 'DATE', 'UT', 'UTC', 'TIMESYS',
    'RA', 'DEC', 'RADESYS', 'EQUINOX',
    'AZIMUTH', 'ZD', 'ALTITUDE', 'HA', 'LST',
    'OBSERVAT', 'TELESCOP', 'INSTRUME',
    'GAINCNFG', 'DEBIAS', 'TRIMMING', 'BURST',
    'CTYPE1', 'CTYPE2', 'CTYPE3',
    'CRVAL1', 'CRVAL2', 'CRVAL3',
    'CRPIX1', 'CRPIX2', 'CRPIX3',
    'CUNIT1', 'CUNIT2', 'CUNIT3',
    'CD1_1', 'CD1_2', 'CD2_1', 'CD2_2',
    'CD1_3', 'CD3_1', 'CD2_3', 'CD3_2', 'CD3_3',
    'LATPOLE', 'LONPOLE', 'VERSION'
  )
  for k in header_keys:
    try:
      new.header.append((k, src[k], src.comments[k]))
    except Exception as e:
      print(str(e), file=sys.stderr)
  new.header.set('CD1_1', new.header['CD1_1'] * args.shrink)
  new.header.set('CD2_2', new.header['CD2_2'] * args.shrink)
  new.header.set('CRPIX1',
                 new.header['CRPIX1'] / args.shrink + 0.5)
  new.header.set('CRPIX2',
                 new.header['CRPIX2'] / args.shrink + 0.5)
  new.header.set('DATE', Time.now().isot)
  new.header.set('CRVAL3', 1.0)
  new.header.set('CRPIX3', 1.0)
  new.header.set('CUNIT3', 'FRAME')
  new.header.set('CD3_3', 1.0)

  new.header.append(
    ('HISTORY', 'background subtracted with a floating Nakamura method.'))

  new.writeto(args.output, overwrite=args.overwrite)


if __name__ == '__main__':
  main()
