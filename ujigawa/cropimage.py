#!/usr/bin/env python
# -*- coding: utf-8 -*-
''' [obsolete] crop out the imaging area '''
from __future__ import print_function, absolute_import, division
from argparse import ArgumentParser as ap
import astropy.io.fits as fits
import os.path as path

if __name__ == '__main__':
  parser = ap(description='[obsolete] crop out the imaging area')

  parser.add_argument('fits', help='input fits file')
  parser.add_argument('out', help='output fits file')
  parser.add_argument('-f', dest='overwrite', action='store_true',
                      help='overwrite a fits image')

  args = parser.parse_args()

  hdu = fits.open(args.fits)

  data = hdu[0].data
  hdr  = hdu[0].header

  if data.ndim == 3:
    z,y,x = data.shape
  elif data.ndim == 2:
    y,x = data.shape
  else:
    raise RuntimeError('input image should be 2- or 3-dimensional')
  if x != 2156 or y != 1128:
    raise RuntimeError('image size {} not supported'.format(data.shape))

  if data.ndim == 3:
    hdu[0].data = data[:,:,156:]
  elif data.ndim == 2:
    hdu[0].data = data[:,156:]
  hdr['CRPIX1'] -= 157
  hdr.add_history(
    '[cropimage] created from {}.'.format(path.basename(args.fits)))

  hdu.writeto(args.out, overwrite=args.overwrite)
