#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import print_function, absolute_import, division
from multiprocessing import Pool
from skimage.morphology import binary_dilation
from skimage.morphology import binary_erosion
from skimage.morphology import remove_small_objects
from skimage.morphology import disk
from skimage.measure import label
import sys,itertools,math
import warnings
import numpy as np
import astropy.io.fits as fits


def presenham(width,step):
  s = 1
  if (step<0): s,step=-1,-step
  A,B,y,arr = step<<1,-(width<<1),0,[0,]
  D = A+(B>>1)
  for i in range(1,width):
    if (D>=0):
      D += B
      y += 1
    D += A
    arr.append(s*y)
  return arr

def medianclean(img):
  nz,ny,nx = img.shape
  med = np.median(img,axis=0)
  m,sig = np.median(med),np.std(med)
  sig = np.std(med[np.abs(med-m)<3*sig])
  sig = np.std(med[np.abs(med-m)<3*sig])
  sig = np.std(med[np.abs(med-m)<3*sig])
  for i in range(nz): img[i] -= med
  med -= m
  med[med<=3*sig] = 0
  med[med>3*sig]  = 1
  med = ~np.array(med,'bool')
  med = binary_erosion(med, disk(5))
  for i in range(nz): img[i] *= med
  return img

def squashimage(img,sqx=2,sqy=2):
  ny,nx = img.shape
  nx,ny = int(nx/sqx), int(ny/sqy)
  sqimg = np.zeros([ny,nx],dtype='float32')
  sqxr = range(sqx)
  sqyr = range(sqy)
  for x,y in itertools.product(sqxr,sqyr):
    sqimg += img[y:(ny*sqy+y):sqy,x:(nx*sqx+x):sqx]
  return sqimg

def meteorreshape(img):
  img -= np.median(img)
  ny,nx = img.shape
  rimg  = np.zeros([361,nx+2*(ny-1)],dtype='float32')
  for n,d in enumerate(np.linspace(-45,45,361)):
    ps = int(ny*math.tan(d*math.pi/180.))
    sft= presenham(ny,ps)
    cnt= np.zeros([nx+2*(ny-1)],dtype='float32')
    for m,s in enumerate(sft):
      rimg[n,(ny+s-1):(ny+s+nx-1)] += img[m,:]
      cnt[(ny+s-1):(ny+s+nx-1)] += 1
    cnt[cnt==0] = 1
    rimg[n,:] /= np.sqrt(cnt)
  rimg = squashimage(rimg,2,1)
  msk = rimg != 0
  std = np.std(rimg[msk])
  rimg[msk] -= np.median(rimg[msk])
  msk = (rimg != 0) & (rimg<3*std)
  std = np.std(rimg[msk])
  msk = (rimg != 0) & (rimg<3*std)
  std = np.std(rimg[msk])
  msk = (rimg != 0) & (rimg<3*std)
  std = np.std(rimg[msk])
  return rimg,std

def zerodiv(A,B):
  B[B==0] = 1
  return A/B
def ginicoeff(X):
  X = np.sort(X-min(X))
  X /= sum(X-X[0])
  return 1.0-2.0*np.sum(np.cumsum(X))/len(X)
def calcparam(img,msk,std,xarr,tarr):
  ny,nx = img.shape
  smed = np.median(img[msk])
  ssig = np.std(img[msk])
  xmsk = img*msk > smed+2*ssig
  tmp = xmsk*(img) - xmsk*(smed+2*ssig)
  if (~xmsk.any()):
    xmsk = img*msk > smed+ssig
    tmp = xmsk*(img) - xmsk*(smed+ssig)
  gx  = np.sum(tmp*xarr)/np.sum(tmp)
  vx  = np.sum(tmp*(xarr-gx)**2)/np.sum(tmp)
  gxv = zerodiv(np.sum(tmp*xarr,axis=1),
          np.sum(tmp,axis=1)).reshape((ny,1))
  gxx = np.mean(zerodiv(
    np.sum((tmp*xarr-tmp*gxv)**2,axis=0),
    np.sum(tmp*tmp,axis=0)))
  gt  = np.sum(tmp*tarr)/np.sum(tmp)
  vt  = np.sum(tmp*(tarr-gt)**2)/np.sum(tmp)
  gtv = zerodiv(np.sum(tmp*tarr,axis=0),
          np.sum(tmp,axis=0)).reshape((1,nx))
  gtt = np.mean(zerodiv(
    np.sum((tmp*tarr-tmp*gtv)**2,axis=1),
    np.sum(tmp*tmp,axis=1)))
  wx  = np.max(xmsk*xarr)-np.min(xmsk*xarr)
  wt  = np.max(xmsk*tarr)-np.min(xmsk*tarr)
  tmp = img[msk]
  snr  = np.max(tmp)/std
  area = np.sum(msk)
  mean = np.mean(tmp)
  gini = ginicoeff(tmp)
  return (gx,gt,snr,vx,vt,gxx,gtt,wx,wt,area,gini)

def detectmeteor(img,frmid):
  img = medianclean(img)
  nz,ny,nx = img.shape
  retarr = []
  retimg = []
  retmsk = []
  param  = []
  for n in range(nz):
    sparam = []
    img[n] = img[n] - np.median(img[n],axis=1).reshape((ny,1))
    ## stack with NAXIS2
    sqimgA = squashimage(img[n],4,4)
    sqimgA,std = meteorreshape(sqimgA)
    sqy,sqx = sqimgA.shape
    xarr = np.linspace(-ny,nx+ny,sqx).reshape((1,sqx))
    tarr = np.linspace(-45,45,sqy).reshape((sqy,1))
    bimgA = np.array(np.zeros([sqy,sqx]),dtype='bool')
    bimgA[sqimgA>5*std] = True
    with warnings.catch_warnings():
      warnings.simplefilter('ignore')
      remove_small_objects(bimgA,9,in_place=True)
    bimgA = binary_dilation(bimgA,disk(5))
    labeled_objects,nlabels = label(bimgA,return_num=True,background=0)
    if (nlabels>0):
      print('{0:3d}: {1} candidates detected.'.format(frmid+1+n,nlabels))
      for sid in range(nlabels):
        msk = labeled_objects==(sid+1)
        (gx,gt,snr,vx,vt,gxx,gtt,wx,wt,area,gini) \
          = calcparam(sqimgA, msk, std, xarr, tarr)
        if ((gxx == 0.0 and wt > 40) or (gxx < 0.001 and wt > 80.0)):
          print('   one candidate rejected (cosmic ray?).')
          print(snr,vx,gxx,vt,wt)
          continue
        sparam.append((frmid+n,gx,1,gt,snr,vx,vt,
                 gxx,gtt,wx,wt,area,gini))

    ## stack with NAXIS1
    sqimgB = squashimage(img[n].T,4,4)
    sqimgB,std = meteorreshape(sqimgB)
    sqy,sqx = sqimgB.shape
    xarr = np.linspace(-nx,ny+nx,sqx).reshape((1,sqx))
    tarr = np.linspace(-45,45,sqy).reshape((sqy,1))
    bimgB = np.array(np.zeros([sqy,sqx]),dtype='bool')
    bimgB[sqimgB>5*std] = True
    with warnings.catch_warnings():
      warnings.simplefilter('ignore')
      remove_small_objects(bimgB,9,in_place=True)
    bimgB = binary_dilation(bimgB,disk(10))
    labeled_objects,nlabels = label(bimgB,return_num=True,background=0)
    if (nlabels>0):
      print('{0:3d}: {1} candidates detected.'.format(frmid+1+n,nlabels))
      for sid in range(nlabels):
        msk = labeled_objects==(sid+1)
        (gx,gt,snr,vx,vt,gxx,gtt,wx,wt,area,gini) \
          = calcparam(sqimgB, msk, std, xarr, tarr)
        gt = 90.0-gt
        if ((gxx == 0.0 and wt > 40) or (gxx < 0.001 and wt > 80.0)):
          print('   one candidate rejected (cosmic ray?).')
          print(snr,vx,gxx,vt,wt)
          continue
        if (abs(gt-90)<1.0 and wt < 2.5 and snr < 10):
          print('   one candidate rejected (horizontal noise?).')
          print(snr,gxx,gtt,wt)
          continue
        sparam.append((frmid+n,1,gx,gt,snr,vx,vt,
                 gxx,gtt,wx,wt,area,gini))

    if (len(sparam)>0):
      retmsk.append(np.concatenate((sqimgA*bimgA,sqimgB*bimgB),axis=1))
      retarr.append(np.concatenate((sqimgA,sqimgB),axis=1))
      retimg.append(img[n])
      param.extend(sparam)

  retimg = np.array(retimg)
  retarr = np.array(retarr)
  print('Frame {0}:{1} completed.'.format(frmid+1,frmid+1+nz))
  return retimg,retarr,retmsk,param

if __name__ == '__main__':
  argc = len(sys.argv)
  if (argc < 3):
    print('Insufficient number of arguments')
    print('Usage: {0} fits output'.format(sys.argv[0]))
    quit()

  (path, out) = sys.argv[1:3]
  f = fits.open(path)

  pool = Pool(None)
  f[0].data = f[0].data.astype('float32')
  hdr   = f[0].header

  nz,ny,nx = f[0].data.shape
  #for n in range(nz):
  #  bias = np.mean(f[0].data[n,:,0:150],axis=1).reshape((1200,1))
  #  f[0].data[n] = (f[0].data[n]-bias)

  #reg = cmos.function.SubRegion(f[0].data.shape, \
  #    cmos.const.image_strX, cmos.const.image_strY, '-')
  hdr['CRVAL3'] = 1
  hdr['CTYPE3'] = 'Number'
  hdr['CUNIT3'] = 'Frame'
  hdr['CDELT3'] = 1
  #f[0].data = cmos.function.subarray(f[0].data, reg);

  results  = list()
  num_list = range(0,f[0].data.shape[0],20)
  for n,i in enumerate(num_list):
    results.append(
      pool.apply_async(func=detectmeteor,
               args=(f[0].data[i:(i+20)],i)))
  pool.close()
  pool.join()

  tmpimages = []
  tmpdetect = []
  tmpdetmsk = []
  objparam  = []
  for n,i in enumerate(num_list):
    rimg,ximg,xmsk,par = results[n].get()
    if (len(par)>0):
      tmpimages.append(rimg)
      tmpdetect.append(ximg)
      tmpdetmsk.append(xmsk)
      objparam.extend(par)

  if (len(tmpimages)==0):
    print('No trail was detected.')
    exit(0)

  cleaned = np.concatenate(tmpimages,axis=0)
  detect  = np.concatenate(tmpdetect,axis=0)
  detmsk  = np.concatenate(tmpdetmsk,axis=0).astype('int16')

  reg = open('{0}.reg'.format(out),'w')
  reg.write('# Region file format: DS9 version 4.0\n')
  reg.write('global color=blue edit=0 move=0 delete=1\n')
  cfrm  = 0
  oldnfrm = -1
  for p in objparam:
    nfrm,x,y,t,snr,vx,vt,xx,tt,wx,wt,area,gini = p
    if (oldnfrm != nfrm):
      cfrm += 1
      oldnfrm = nfrm
    u = math.pi*(t+90.0)/180.0
    A,B,C = math.sin(u), -math.cos(u),y*math.cos(u)-x*math.sin(u)
    x0,y0 = 1000.5,564.5
    x = (-A*B*y0+B*B*x0-A*C)/(A*A+B*B)
    y = (A*A*y0-A*B*x0-B*C)/(A*A+B*B)
    reg.write('\n')
    reg.write('# Frame: {0}\n'.format(cfrm))
    reg.write('#   original frame: {0}\n'.format(nfrm+1))
    reg.write('#   position: ({0},{1}) angle: {2}\n'.format(x,y,t))
    reg.write('#   SNR: {0}\n'.format(snr))
    reg.write('#   total physical var: {0}\n'.format(vx))
    reg.write('#   local physical var: {0}\n'.format(xx))
    reg.write('#   pysical size: {0}\n'.format(wx))
    reg.write('#   total angular var: {0}\n'.format(vt))
    reg.write('#   local angular var: {0}\n'.format(tt))
    reg.write('#   angular size: {0}\n'.format(wt))
    reg.write('#   masked area: {0}\n'.format(area))
    reg.write('#   ginicoeff: {0}\n'.format(gini))
    reg.write('image;box {0} {1} 30 3000 {2}'.format(x,y,t))
    reg.write('  # tag = {{frame {0}}}\n'.format(cfrm))
  reg.close()

  f[0].data = cleaned
  f.append(fits.ImageHDU(data=detmsk))
  f.append(fits.ImageHDU(data=detect))
  f.writeto(out, overwrite=True)
