#!/usr/bin/env python
# -*- coding: utf-8 -*-
from argparse import ArgumentParser as ap
import astropy.io.fits as fits
import numpy as np
import os.path as path
from ujigawa.misc import command_line_function


def slantCube(cube, deg):
  ''' Skew a 3-dimensional image by a given degree.

  Args:
    data: an array-like object that contains image data.
    deg: a skew angle in units of degree.

  Returns:
    A numpy array skewed by the given angle.
  '''
  z,y,x = cube.shape
  xo,yo = np.meshgrid(range(x), range(y))
  xt = np.mod(xo+yo*np.tan(np.pi*(deg)/180.),x)
  xf,x1 = np.modf(xt)
  x1 = x1.astype('int32')
  x2 = np.mod(x1+1,x)
  yi = yo.astype('int32')
  return (1.0-xf)*cube[:,yi,x1]+xf*cube[:,yi,x2]


def slantImage(image, deg):
  ''' Skew a 2-dimensional image by a given degree.

  Args:
    data: an array-like object that contains image data.
    deg: a skew angle in units of degree.

  Returns:
    A numpy array skewed by the given angle.
  '''
  y,x = image.shape
  xo,yo = np.meshgrid(range(x), range(y))
  xt = np.mod(xo+yo*np.tan(np.pi*(deg)/180.),x)
  xf,x1 = np.modf(xt)
  x1 = x1.astype('int32')
  x2 = np.mod(x1+1,x)
  yi = yo.astype('int32')
  return (1.0-xf)*image[yi,x1]+xf*image[yi,x2]


def slantMatrix(data, deg):
  ''' Skew an image by a given degree.

  Args:
    data: an array-like object that contains image data.
    deg: a skew angle in units of degree.

  Returns:
    A numpy array skewed by the given angle.
  '''
  deg = ((deg+45+180) % 180) -45
  if data.ndim == 3:
    return slantCube(data, deg)
  elif data.ndim == 2:
    return slantImage(data, deg)


@command_line_function
def main(*argv):
  parser = ap(description='Obtian a slanted image.')

  parser.add_argument('fits',  help='input fits file')
  parser.add_argument('angle', type=float, help='slant angle in degree')
  parser.add_argument('out',   help='output fits file')
  parser.add_argument('-f', dest='overwrite', action='store_true',
                      help='overwrite a fits image')

  args = parser.parse_args(argv[1:])

  hdu = fits.open(args.fits)
  hdu[0].data = slantMatrix(hdu[0].data, deg)
  hdu[0].header.add_history(
    '[imslant] created from.'.format(args.fits))
  hdu[0].header.add_history(
    '[imslant] slant angle: {} deg.'.format(args.angle))

  hdu.writeto(args.out, overwrite=args.overwrite)


if __name__ == '__main__':
  main()
