#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import print_function, division
from argparse import ArgumentParser as ap
from scipy.ndimage.filters import gaussian_filter as gaussian
import astropy.io.fits as fits
import numpy as np
from ujigawa.misc import calc_time_shift, sigma_clipped_mean
from ujigawa.misc import search_radius_mask, gaussian_kernel
from ujigawa.misc import command_line_function


@command_line_function
def main(*argv):
  parser = ap()
  parser.add_argument('fits', help='input FITS image file')
  parser.add_argument('x', type=float, help='NAXIS1 location of the target')
  parser.add_argument('y', type=float, help='NAXIS2 location of the target')
  parser.add_argument('z', type=int, nargs='?', default=1,
                      help='NAXIS3 location of the target')

  parser.add_argument('--output', dest='output', action='store',
                      help='output flux table file')
  parser.add_argument('-r', dest='radius', action='store', type=float,
                      default=5.0, help='centering search radius')
  parser.add_argument('--gauss', dest='gauss', action='store', type=float,
                      default=2.0, help='kernel size of image smoothing')
  parser.add_argument('--sigma', dest='sigma', action='store', type=float,
                      default=3.0, help='sigma clipping value')
  parser.add_argument('--niter', dest='niter', action='store', type=int,
                      default=5, help='number of iteration in sigma-clipping')
  parser.add_argument('--crval3', dest='crval3', action='store', type=float,
                      default=0.0, help='specify CRVAL3')
  parser.add_argument('--crpix3', dest='crpix3', action='store', type=float,
                      default=1.0, help='specify CRPIX3')
  parser.add_argument('--fps', dest='fps', action='store', type=float,
                      default=2.0, help='specify frame-per-second')
  # parser.add_argument('-f', dest='overwrite', action='store_true',
  #                     help='overwrite output file')

  args = parser.parse_args(argv[1:])
  x,y,z = args.x,args.y,args.z
  s,n,r = args.sigma,args.niter,args.radius

  hdu = fits.open(args.fits)[0]
  img = hdu.data
  if img.ndim == 2:
    ny,nx = img.shape
    img = img.reshape((1,ny,nx))

  t0 = hdu.header.get('CRVAL3', args.crval3)
  dt = hdu.header.get('TFRAME', 1.0/args.fps)
  n0 = hdu.header.get('CRPIX3', args.crpix3)-1
  nz,ny,nx = img.shape
  Ax,Ay = np.meshgrid(range(nx),range(ny))

  gx,gy = args.x,args.y

  try:
    p = img[z-1]
    q = gaussian(p, sigma=args.gauss)
    m,s = sigma_clipped_mean(p, args.sigma, args.niter)
    mask = search_radius_mask(q, gx, gy, args.radius)
    d = np.sum(np.abs(q-m)*mask)
    gx = np.sum(Ax*np.abs(q-m)*mask)/d+1
    gy = np.sum(Ay*np.abs(q-m)*mask)/d+1
    xx = np.sum((Ax-gx+1)*(Ax-gx+1)*np.abs(q-m)*mask)/d
    yy = np.sum((Ay-gy+1)*(Ay-gy+1)*np.abs(q-m)*mask)/d
    fwhm = np.sqrt((2*np.log(2))*(xx+yy))
    mask = search_radius_mask(q, gx, gy, np.sqrt(args.radius**2+fwhm**2))
    d = np.sum(np.abs(q-m)*mask)
    gx = np.sum(Ax*np.abs(q-m)*mask)/d+1
    xx = np.sum((Ax-gx+1)*(Ax-gx+1)*np.abs(q-m)*mask)/d
    yy = np.sum((Ay-gy+1)*(Ay-gy+1)*np.abs(q-m)*mask)/d
    fwhm = np.sqrt((2*np.log(2))*(xx+yy))
    k = gaussian_kernel(q,gx,gy,fwhm)
    flux = np.sum(k*(q-m))
    err = s*np.sqrt(np.sum(k*k))
    ts = calc_time_shift(q, gx, gy)
    if np.isnan(flux) or np.isnan(err) or np.isnan(fwhm):
      raise RuntimeError('Failed to detect object at frame {}.'.format(n))
    tn = t0+dt*(z-1-n0+0.5)+ts
    phot = [tn, gx, gy,flux,err,fwhm]
  finally:
    print(*phot)
    if args.output is not None:
      phot = np.array(phot)
      np.savetxt(args.output, phot)


if __name__ == '__main__' :
  main()
