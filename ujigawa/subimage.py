#!/usr/bin/env python
# -*- coding: utf-8 -*-
from argparse import ArgumentParser as ap
import astropy.io.fits as fits
import os.path as path
from ujigawa.misc import crop_image
from ujigawa.misc import command_line_function


@command_line_function
def main(*argv):
  parser = ap(description='Calculate statistics along with NAXIS3')

  parser.add_argument('fits', help='input fits file')
  parser.add_argument('x_range', help='NAXIS1 range in [begin]:[end]')
  parser.add_argument('y_range', help='NAXIS2 range in [begin]:[end]')
  parser.add_argument('z_range', help='NAXIS3 range in [begin]:[end]')
  parser.add_argument('out', help='output fits file')
  parser.add_argument('-f', dest='overwrite', action='store_true',
                      help='overwrite a fits image')

  args = parser.parse_args(argv[1:])

  regions = (args.z_range, args.y_range, args.x_range)
  hdu = fits.open(args.fits)
  data, hdr = hdu[0].data, hdu[0].header

  if data.ndim == 2:
    y,x = data.shape
    data = data.reshape(1,y,x)

  reg, data = crop_image(regions, data)

  if data.shape[0] == 1:
    z,y,x = data.shape
    data = data.reshape(y,x)

  hdr['CRPIX1'] -= reg[2].start
  hdr['CRPIX2'] -= reg[1].start
  hdr['CRPIX3'] -= reg[0].start

  hdr.add_history(
    '[subimage] created from {}'.format(path.basename(args.fits)))
  hdr.add_history(
    '[subimage] cropped region: (z, y, x) = ({}, {}, {})'.format(*regions))

  hdu[0].data   = data
  hdu[0].header = hdr
  hdu[0].writeto(args.out, overwrite=args.overwrite)


if __name__ == '__main__':
  main()
