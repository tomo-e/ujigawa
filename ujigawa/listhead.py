#!/usr/bin/env python
# -*- coding: utf-8 -*-
from argparse import ArgumentParser as ap
import astropy.io.fits as fits
import numpy as np
import os.path as path
from ujigawa.misc import command_line_function


@command_line_function
def main(*argv):
  parser = ap(description='List header information as a bar-separated table')

  parser.add_argument('keywords', type=str,
                      help='header KEYWORDS (comma separated string)')
  parser.add_argument('fits', type=str, nargs='+',
                      help='fits files')
  parser.add_argument('--with-filename', dest='filename', action='store_true',
                      help='display filename')
  parser.add_argument('--with-header', dest='with_header', action='store_true',
                      help='display table header')

  args = parser.parse_args(argv[1:])

  keys = args.keywords.split(',')

  if args.with_header is True:
    if args.filename is True:
      print('{}|{}'.format('FILENAME','|'.join(keys)))
    else:
      print('|'.join(keys))

  for f in args.fits:
    hdu0 = fits.open(f)[0]
    header = hdu0.header
    vals = [str(header.get(k,None)) for k in keys]
    if args.filename is True:
      print('{}|{}'.format(path.basename(f),'|'.join(vals)))
    else:
      print('|'.join(vals))


if __name__ == '__main__':
  main()
