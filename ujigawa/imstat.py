#!/usr/bin/env python
# -*- coding: utf-8 -*-
from argparse import ArgumentParser as ap
import astropy.io.fits as fits
import numpy as np
from ujigawa.misc import statistics, crop_image
from ujigawa.misc import command_line_function


@command_line_function
def main(*argv):
  parser = ap(description='Calculate statistics of an image.')

  parser.add_argument('method', choices=('mean','median','max','min','std'),
                      help='statistics to be calculated')
  parser.add_argument('fits', help='input fits file')
  parser.add_argument('x_range', nargs='?', default=':',
                      help='NAXIS1 range in [begin]:[end]')
  parser.add_argument('y_range', nargs='?', default=':',
                      help='NAXIS2 range in [begin]:[end]')
  parser.add_argument('z_range', nargs='?', default=':',
                      help='NAXIS3 range in [begin]:[end]')

  args = parser.parse_args(argv[1:])
  method = statistics(args.method)
  regions = (args.z_range, args.y_range, args.x_range)

  hdu = fits.open(args.fits)
  data, hdr = hdu[0].data, hdu[0].header

  reg, data = crop_image(regions, data)

  print(method(data))


if __name__ == '__main__':
  main()
