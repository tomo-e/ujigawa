#!/usr/bin/env python
# -*- coding: utf-8 -*-
from argparse import ArgumentParser as ap
import astropy.io.fits as fits
import numpy as np
import os.path as path
from ujigawa.misc import command_line_function


@command_line_function
def main(*argv):
  parser = ap(description='[obsolete] Remove the channel-bias pattern')

  parser.add_argument('fits', help='input fits file')
  parser.add_argument('out', help='output fits file')
  parser.add_argument('-f', dest='overwrite', action='store_true',
                      help='overwrite a fits image')

  args = parser.parse_args(argv[1:])

  hdu = fits.open(args.fits)

  hdu[0].data = hdu[0].data.astype('float32')
  data = hdu[0].data
  hdr  = hdu[0].header
  if data.ndim == 2:
    y,x = data.shape
    z = 1
    data = data.reshape((z,y,x))
  elif data.ndim == 3:
    z,y,x = data.shape

  for n in range(z):
    d = []
    for m in range(4,60,4):
      d.append(data[n,m:(m+4),:])
    d = np.median(np.array(d), axis=0)
    for m in range(0,y,4):
      data[n,m:(m+4),:] -= d

  if z==1: data = data.reshape((y,x))

  hdu[0].data = data
  hdu[0].header.add_history(
    '[debias] created from {}'.format(path.basename(args.fits)))

  hdu.writeto(args.out, overwrite=args.overwrite)


if __name__ == '__main__':
  main()
