#!/usr/bin/env python
# -*- coding: utf-8 -*-
import numpy as np
import re


def command_line_function(func):
  ''' A decolator to make scripts work as console commands.

  Paramters:
    func: A function to be wrapped.

  Return:
    A function with command line arguments.
  '''
  def wrap_function():
    import sys
    return func(*sys.argv)
  return wrap_function


def search_radius_mask(img, x, y, r):
  ''' Create a boolean mask image.

  Parameters:
    img: a ndarray image with (nx,ny) dimension
    x: coordinate in NAXIS1 in the FITS image (1-origin)
    y: coordinate in NAXIS2 in the FITS image (1-origin)
    r: a search radius in units of pixels

  Return:
    A boolean ndarray, which contains True around the specified point.
  '''
  ny,nx = img.shape
  Ax,Ay = np.meshgrid(range(nx),range(ny))
  return (Ax-x+1)**2+(Ay-y+1)**2 <= r*r


def gaussian_kernel(img, x, y, w):
  ''' Generate a gaussian kernel for photometry.

  Parameters:
    img: a ndarray image with (nx,ny) dimension
    x: coordinate in NAXIS1 in the FITS image (1-origin)
    y: coordinate in NAXIS2 in the FITS image (1-origin)
    w: an FWHM of a Gaussian kernel in units of pixels

  Return:
    A float ndarray, which contains a normalized gaussian kernel.
  '''
  ny,nx = img.shape
  Ax,Ay = np.meshgrid(range(nx),range(ny))
  d = w*w/(4*np.log(2))
  D = (Ax-x+1)**2+(Ay-y+1)**2
  k = np.exp(-D/d)
  return k / np.sum(k)


def calc_time_shift(img, x, y):
  ''' Calculate time-shift from the (1,1) pixel.

  Parameters:
    img: an nd array with (nx,ny) dimension
    x: NAXIS1 coordinate of the target
    y: NAXIS2 coordinate of the target

  Return:
    The timeshift value in units of second.
  '''
  ny,nx = img.shape
  if nx==2000 and ny==1128:
    return (930+2160+1000+np.floor(nx/4)*25)*np.floor(y/4)*100e-9
  else:
    return (930+2160+975+np.floor(nx/4)*25)*np.floor(y/4)*100e-9


def sigma_clipped_mean(img, n, niter):
  ''' Calculate a sigma-clipped mean value from an image.

  Parameters:
    img: an ndarray image with (nx,ny) dimension
    n: a threshold of sigma-clipping
    niter: the number of iterations

  Return:
    A tuple of the sigma-clipped mean and sigma-clipped standard deviation.
  '''
  f = np.full(img.shape, True)
  for i in range(niter):
    m,s = np.median(img[f]),np.std(img[f])
    f = np.abs(img-m) < n*s
  return np.mean(img[f]),np.std(img[f])


def statistics(method):
  ''' Get a function to calculate statistics.

  Parameters:
    method: A name of statistics.

  Return:
    A numpy function to calculate statistics from ndarray.
  '''
  if method.lower() not in ('mean','median','max','min','std'):
    raise RuntimeError('unknown statistics "{}" requested'.format(method))
  if method.lower() == 'mean':
    return np.mean
  elif method.lower() == 'median':
    return np.median
  elif method.lower() == 'max':
    return np.max
  elif method.lower() == 'min':
    return np.min
  elif method.lower() == 'std':
    return np.std


def crop_image(regions, image):
  ''' Get an axis-range from a given text.

  Parameters:
    regions: A list containing strings to specify axis-ranges.
    image: N-dimensional array.

  Return:
    A sub-array cropped based on the specified axis-ranges.
  '''

  #if len(regions) != len(image.shape):
  #  raise RuntimeError('region info is not consistent with image shape.')

  parser = re.compile(r'^([1-9][0-9]*)?(:)?([1-9][0-9]*)?$')
  safeInt = lambda s, n: int(s) if s is not None else n
  getSlice = lambda q, n: slice(safeInt(q[0], 1)-1, safeInt(q[0], n)) \
             if q[1] is None else slice(safeInt(q[0], 1)-1, safeInt(q[2], n))

  if image.ndim == 2:
    y,x = image.shape
    yres = parser.match(regions[0])
    xres = parser.match(regions[1])
    if yres is None: raise RuntimeError('failed to parse NAXIS2 range.')
    if xres is None: raise RuntimeError('failed to parse NAXIS1 range.')

    yobj, xobj = yres.groups(), xres.groups()
    yr, xr = getSlice(yobj, y), getSlice(xobj, x)

    return (yr,xr), image[yr,xr]

  elif image.ndim == 3:
    z,y,x = image.shape
    zres = parser.match(regions[0])
    yres = parser.match(regions[1])
    xres = parser.match(regions[2])
    if zres is None: raise RuntimeError('failed to parse NAXIS3 range.')
    if yres is None: raise RuntimeError('failed to parse NAXIS2 range.')
    if xres is None: raise RuntimeError('failed to parse NAXIS1 range.')

    zobj, yobj, xobj = zres.groups(), yres.groups(), xres.groups()
    zr, yr, xr = getSlice(zobj, z), getSlice(yobj, y), getSlice(xobj, x)

    return (zr,yr,xr), image[zr,yr,xr]

  else:
    raise RuntimeError('4- or higher-dimensional array is not supported.')
