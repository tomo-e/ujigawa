#!/usr/bin/env python
# -*- coding: utf-8 -*-
from argparse import ArgumentParser as ap
from astropy.wcs import WCS as wcs
from astropy.coordinates import SkyCoord
import astropy.io.fits as fits
import astropy.units as u
import numpy as np
import os.path as path
from ujigawa.misc import command_line_function


__naxis3_keywords = (
  'NAXIS3', 'CTYPE3', 'CRPIX3', 'CRVAL3', 'CUNIT3',
  'CD1_3', 'CD2_3', 'CD3_3', 'CD3_2', 'CD3_1',
)


@command_line_function
def main(*argv):
  parser = ap(
    description='Detect moving objects with the shift-and-add procedure')

  parser.add_argument('fits', help='input fits file')
  parser.add_argument('v_ra', type=float,
                      help='RA velocity in arcsec/ho')
  parser.add_argument('v_dec', type=float,
                      help='DEC velocity in arcsec/ho')
  parser.add_argument('output', help='output fits file')
  parser.add_argument('-f', dest='overwrite', action='store_true',
                      help='overwrite a fits image')

  args = parser.parse_args(argv[1:])

  hdu = fits.open(args.fits)
  img = hdu[0].data
  hdr = hdu[0].header

  z,y,x = img.shape
  dt = hdr.get('TFRAME', 0.5)
  hdr.set('NAXIS', 2)
  for key in __naxis3_keywords: hdr.remove(key, ignore_missing=True)
  w = wcs(header=hdr)
  radec = w.all_pix2world([[x/2.0, y/2.0+100.], [x/2.0, y/2.0-100.]], 1)
  p0 = SkyCoord(ra=radec[0][0], dec=radec[0][1], unit=(u.deg, u.deg))
  p1 = SkyCoord(ra=radec[1][0], dec=radec[1][1], unit=(u.deg, u.deg))
  pa = p1.position_angle(p0)

  v_ax1 = np.cos(pa)*args.v_ra + np.sin(pa)*args.v_dec
  v_ax2 = - np.sin(pa)*args.v_ra - np.cos(pa)*args.v_dec

  add = np.zeros((y,x))
  for n in range(z):
    ax1 = int(n*v_ax1/3600.*0.5/1.189)
    ax2 = int(n*v_ax2/3600.*0.5/1.189)
    tmp = np.roll(img[n], ax2, axis=0)
    tmp = np.roll(tmp, ax1, axis=1)
    add += tmp/z

  hdu[0].data = add
  hdu[0].header.add_history(
    '[shiftadd] created from {}.'.format(path.basename(args.fits)))
  hdu[0].header.add_history(
    '[shiftadd] (v_ra, v_dec) = ({}, {}).'.format(args.v_ra, args.v_dec))

  hdu.writeto(args.output, overwrite=args.overwrite)


if __name__ == '__main__':
  main()
