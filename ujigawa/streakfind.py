#!/usr/bin/env python
# -*- coding: utf-8 -*-
from argparse import ArgumentParser as ap
from sklearn.utils.extmath import randomized_svd
from skimage.morphology import disk, dilation, binary_dilation
from skimage.measure import label
from scipy.stats import norm
from astropy.table import Table
from astropy.time import Time, TimeDelta
import astropy.io.fits as fits
import astropy.units as u
import numpy as np
import warnings, sys
from ujigawa.misc import command_line_function


def safe_divide(image, weight):
  ''' Zero-division free array division

  Divide two arrays without the division-by-zero error.

  Args:
    image: an ndarray, which is to be divided
    weight: an ndarray, which is to be a denominator
  '''
  weight[weight==0] = 1.0
  return image/weight


def calc_param(image, mask):
  y,x = image.shape
  peak = np.max(image*mask)
  nx, ny = np.arange(x), np.arange(y)
  mx, my = np.meshgrid(nx, ny)

  mask = binary_dilation(mask, disk(2))
  snr = np.sum(image * mask)
  area = int(np.sum(mask))
  far = 1-norm.cdf(snr, loc=0.0, scale=area)

  mask = image*mask > peak/2.0
  fwhm = int(np.sum(mask))
  wx = int(np.max(mx[mask]) - np.min(mx[mask]) + 1.0)
  wy = int(np.max(my[mask]) - np.min(my[mask]) + 1.0)

  gx = np.sum(image * mx * mask)/np.sum(image * mask)
  gy = np.sum(image * my * mask)/np.sum(image * mask)
  xx = np.sum(image * mx * mx * mask)/np.sum(image * mask)
  yy = np.sum(image * my * my * mask)/np.sum(image * mask)
  vx, vy = xx - gx*gx, yy - gy*gy

  return (snr,far,peak,area,fwhm,wx,wy,gx+1,gy+1,vx,vy)


class FrameList(list):
  def __init__(self, *args, **options):
    super(FrameList, self).__init__(*args, **options)


class ParameterList(object):
  def __init__(self):
    self.__param_dict__ = dict()

  def append(self, n, param):
    if not n in self.__param_dict__:
      self.__param_dict__[n] = list()
      self.__param_dict__[n].append(param)
    else:
      self.__param_dict__[n].append(param)

  def __getitem__(self, n):
    return self.__param_dict__[n]

  def __contains__(self, n):
    return n in self.__param_dict__

  def __len__(self):
    return len(self.__param_dict__)


def line_detection(image, sigma_threshold=5.0,
                   false_alarm=0.01, verbose=False):
  bright_pixels = image > sigma_threshold
  detected = FrameList()
  params = ParameterList()
  for n in range(len(image)):
    dilated = binary_dilation(bright_pixels[n], disk(1))
    objects,nlabels = label(dilated,return_num=True,background=0)
    if nlabels > 0:
      if verbose: print('Frame #{}'.format(n+1), file=sys.stderr)
      for oid in range(nlabels):
        mask = (objects == (oid+1))
        try:
          param = calc_param(image[n], mask)
        except Exception as e:
          print(str(e), file=sys.stderr)
          continue
        snr,far,peak,area,fwhm,wx,wy,gx,gy,vx,vy = param
        if far <  false_alarm:
          detected.append(n)
          params.append(n, param)
          if verbose:
            print('candidate#{} detected'.format(oid), file=sys.stderr)
            print('    FAR: {}, (x, y) = ({}, {})'.format(far, gx,gy),
                  file=sys.stderr)
        elif verbose:
          print('candidate#{} rejected'.format(oid), file=sys.stderr)
          print('    FAR: {}, (x, y) = ({}, {})'.format(far, gx,gy),
                file=sys.stderr)
      if verbose: print('', file=sys.stderr)
  detected = np.unique(np.array(detected, dtype=np.int16))
  return detected, params


def false_alarm_rate(arg):
  val = float(arg)
  if val <= 0 or 1 <= val:
    raise RuntimeError('false alarm rate should be in (0, 1)')
  return val


@command_line_function
def main(*argv):
  parser = ap(
    description='Detect Faint Meteors using Hough Transformation')

  parser.add_argument('fits', help='input fits image')
  parser.add_argument('base', help='base of output fits file names')

  parser.add_argument('-r','--far', dest='false_alarm', action='store',
                      type=false_alarm_rate, default=0.01,
                      help='false alarm rate [default=0.01]')

  parser.add_argument('-v', dest='verbose', action='store_true',
                      help='enable verbose output')
  parser.add_argument('-f', dest='overwrite', action='store_true',
                      help='overwrite the output fits file if exists')

  args = parser.parse_args(argv[1:])

  hdu = fits.open(args.fits)
  hough = hdu[0].data
  if hough.ndim != 3:
    raise RuntimeError('fits file should contain a 3d-image')

  z,y,x = hough.shape
  hough = hough / np.std(hough.reshape((z,y*x)),axis=1).reshape((z,1,1))

  detected_frames, params = line_detection(
    hough, false_alarm=args.false_alarm, verbose=args.verbose)

  detected = fits.HDUList([fits.PrimaryHDU(), fits.TableHDU()])
  header_keys = (
    'OBJECT', 'PROJECT', 'OBSERVER',
    'EXPTIME', 'TELAPSE', 'EXPTIME1', 'TFRAME', 'DATA-FPS',
    'DATE', 'UTC', 'TIMESYS',
    'RA', 'DEC', 'RADESYS', 'EQUINOX',
    'AZIMUTH', 'ZD', 'ALTITUDE', 'HA', 'LST',
    'OBSERVAT', 'TELESCOP', 'INSTRUME',
    'GAINCNFG', 'DEBIAS', 'TRIMMING', 'BURST',
    'CTYPE1', 'CTYPE2', 'CTYPE3',
    'CRVAL1', 'CRVAL2', 'CRVAL3',
    'CRPIX1', 'CRPIX2', 'CRPIX3',
    'CUNIT1', 'CUNIT2', 'CUNIT3',
    'CD1_1', 'CD1_2', 'CD2_1', 'CD2_2',
    'CD1_3', 'CD3_1', 'CD2_3', 'CD3_2', 'CD3_3',
    'LATPOLE', 'LONPOLE', 'VERSION',
  )
  src = hdu[0].header
  for key in header_keys:
    try:
      detected[0].header.append((key, src[key], src.comments[key]))
    except Exception as e:
      print(str(e), file=sys.stderr)
  time_zero = Time(src['UTC'], scale='utc')
  time_tics = TimeDelta(float(src['TFRAME'])*u.second)

  column_names = (
    'SNR',
    'FAR',
    'peak_SNR',
    'area',
    'half_area',
    'physical_size',
    'angular_size',
    'x_position',
    'y_position',
    'x_var',
    'y_var',
  )
  data_types = (
    'f4', # SNR
    'f4', # FAR
    'f4', # Peak
    'i2', # Integ
    'i2', # Area
    'i2', # X-Size
    'i2', # Y-Size
    'f4', # X
    'f4', # Y
    'f4', # VX
    'f4', # VY
  )

  if len(detected_frames) > 0:
    for n in detected_frames:
      detected[0].data = hough[n]
      detected[0].header.set('DATE', Time.now().isot)
      time_stamp = time_zero + n*time_tics
      detected[0].header.set('UTC', time_stamp.isot)

      events = Table(rows=params[n], names=column_names, dtype=data_types)
      detected[1] = fits.BinTableHDU(data=events, name='events')

      filename='{}_{:03d}.fits'.format(args.base, n+1)
      detected.writeto(filename, overwrite=args.overwrite)
  else:
    print('no stread detected.', file=sys.stderr)


if __name__ == '__main__':
  main()
