#!/usr/bin/env python
# -*- coding: utf-8 -*-
from argparse import ArgumentParser as ap
import astropy.io.fits as fits
import numpy as np
import os.path as path
from ujigawa.misc import command_line_function


@command_line_function
def main(*argv):
  parser = ap(description='Arithmetic operations on images.')

  parser.add_argument('lhs', help='left operand (image)')
  parser.add_argument('op', metavar='op', choices=('+','-','*','x','/'),
                      help='operator {+, -, *, x, /}')
  parser.add_argument('rhs', help='right operand (image or number)')
  parser.add_argument('out', help='output fits file')
  parser.add_argument('-f', dest='overwrite', action='store_true',
                      help='overwrite a fits image')

  args = parser.parse_args()

  hdu = fits.open(args.lhs)
  lhs = hdu[0].data.astype(np.float32)
  try:
    tmp = fits.open(args.rhs)
    rhs = tmp[0].data.astype(np.float32)
    if lhs.ndim == 3 and rhs.ndim == 2:
      y,x = rhs.shape
      rhs = rhs.reshape(1,y,x)
    elif lhs.ndim == 2 and rhs.ndim == 3:
      y,x = lhs.shape
      lhs = lhs.reshape(1,y,x)
  except Exception as e:
    # failed to open a fits file -> try float number
    rhs = float(args.rhs)

  if   args.op == '+':
    lhs = lhs + rhs
  elif args.op == '-':
    lhs = lhs - rhs
  elif args.op == '*' or args.op=='x':
    lhs = lhs * rhs
  elif args.op == '/':
    lhs = lhs / rhs

  hdu[0].data = lhs
  hdu[0].header.add_history(
    '[imarith] left hand side : {}'.format(path.basename(args.lhs)))
  hdu[0].header.add_history(
    '[imarith] right hand side: {}'.format(path.basename(args.rhs)))
  hdu[0].header.add_history(
    '[imarith] operator       : {}'.format(args.op))

  hdu.writeto(args.out, overwrite=args.overwrite)


if __name__ == '__main__':
  main()
