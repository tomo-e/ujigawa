#!/usr/bin/env python
# -*- coding: utf-8 -*-
from argparse import ArgumentParser as ap
from sklearn.utils.extmath import randomized_svd
from skimage.morphology import disk, dilation, binary_dilation
from skimage.measure import label
from numba import jit,int16,float32
from astropy.time import Time
import numpy as np
import astropy.io.fits as fits
import warnings, sys
from ujigawa.misc import command_line_function


@jit(float32[:,:,:](float32[:,:,:],int16))
def squish_image(image, shrink):
  ''' Squish the size of array

  Return a shrinked image array or cube. The x- and y-axes are shrinked by
  a factor of `shrink`.

  Parameters:
    image: ndarray containing a 2d- or 3d-image
    shrink: squishing factor (should be a positive integer)
  '''
  z,y,x = image.shape
  ny,nx = y//shrink, x//shrink
  image = image[:,:ny*shrink,:nx*shrink]
  new_image = np.zeros((z,ny,nx), dtype=np.float32)
  for my in range(shrink):
    for mx in range(shrink):
      new_image += image[:,my::shrink,mx::shrink]
  return new_image


def rotated_index(nx, ny, theta, coarse):
  '''

  Parameters:
    nx: the NAXIS1 size of the input array
    ny: the NAXIS2 size of the input array
    theta: rotation angle in radian
    coarse:
  '''
  x = np.arange(nx, dtype=np.float32) - nx/2.0
  y = np.arange(ny, dtype=np.float32) - ny/2.0
  s = np.sqrt(ny**2+nx**2)+2
  xv, yv = np.meshgrid(x,y)
  X = np.zeros(xv.size, dtype=np.int16)
  sinT, cosT = np.sin(theta), np.cos(theta)
  for n,P in enumerate(zip(yv.flat,xv.flat)):
    X[n] = int(np.round((cosT*P[1] - sinT*P[0] + s/2.0)/coarse))
  return X


def safe_divide(image, weight):
  ''' Zero-division free array division

  Divide two arrays without the division-by-zero error.

  Parameters:
    image: an ndarray, which is to be divided
    weight: an ndarray, which is to be a denominator
  '''
  weight[weight==0] = 1.0
  return image/weight


def normalize_cube(cube):
  ''' Normalize data cube

  Parameters:
    cube: an ndarray, which contains a 3-dimensional array
  '''
  z,y,x = cube.shape
  sigmap = np.std(cube,axis=0).reshape((1,y,x))
  sigmap[sigmap==0] = 1.0
  cube = cube / sigmap
  sigvec = np.std(cube.reshape((z,y*x)),axis=1).reshape((z,1,1))
  cube = cube / sigvec
  return cube


@jit(float32[:](float32[:],int16[:],float32[:]), nopython=True)
def accumulate_by_index(canvas, index, array):
  ''' Accumulate array values on canvas

  Distribute values in `array` on the `canvas` array based on the given index.

  Parameters:
    canvas: an ndarray, which is a canvas array
    index: an int16 ndarray, containing an index array
    array: an ndarray, containing values to be distributed
  '''
  for i,x in zip(index,array):
    canvas[i] += x
  return canvas


def hough_transform(image, coarse=2, tics=1.0, angle_s=-5.0, angle_e=185.0):
  ''' Hough Transformation

  Return a Hough-transformed image array or cube.

  Parameters:
    image: ndarray containing a 2d- or 3d- image
    coarse: sparsity factor (should be a positive integer, optional)
    tics: rotation angle tics in degree (optional)
    angle: begining and end angles (optional)
  '''
  theta = np.arange(angle_s, angle_e, tics)/180.0*np.pi
  n = theta.size
  z,y,x = image.shape
  l = int(np.ceil(np.sqrt(y**2+x**2)/coarse)+2)
  canvas = np.zeros((z,n,l), dtype=np.float32)
  weight = np.zeros((z,n,l), dtype=np.float32)
  unity = np.ones(y*x, dtype=np.float32)
  for nz in range(z):
    for m,t in enumerate(theta):
      index = rotated_index(x, y, t, coarse)
      array = image[nz].flatten()
      canvas[nz,m] = accumulate_by_index(canvas[nz,m], index, array)
      weight[nz,m] = accumulate_by_index(weight[nz,m], index, unity)
  return canvas, weight


def low_rank_approx(array, rank):
  ''' Compute a low-rank approximation

  Return a low-rank approximation of the given matrix. The rank of the
  approximated matrix is specificed by `rank`.

  Parameters:
    array: an ndarray to be approximated
    rank: the rank of the approximated matrix
  '''
  u,s,v = randomized_svd(array, n_components=rank)
  approx = u.dot(np.diag(s)).dot(v)
  return approx


def soft_threshold(array, threshold):
  ''' Compute a sparse matrix by the soft-thresholding method

  Parameters:
    array: an ndarray to be converted
    threshold: an threshold level
  '''
  frame,pix = array.shape
  threshold = threshold.reshape((frame,1))
  absolute = np.abs(array)
  absolute = absolute - threshold
  absolute[absolute<0.0] = 0.0
  return np.sign(array)*absolute


def get_sparse_matrix_by_soft_godec(array, rank, sigma, max_iter=10):
  approx = np.zeros_like(array, dtype=np.float32)
  sparse = np.zeros_like(array, dtype=np.float32)

  for n in range(max_iter):
    approx = low_rank_approx(array-sparse, rank=rank)
    stddev = np.std(array-approx-sparse, axis=1)
    sparse = soft_threshold(array-approx, threshold=sigma*stddev)
  return array-approx


@command_line_function
def main(*argv):
  parser = ap(
    description='Detect Faint Meteors using Hough Transformation')

  parser.add_argument('fits', help='input fits image')
  parser.add_argument('output', help='output fits file name')

  parser.add_argument('--shrink', action='store', type=int, default=4,
                      help='image squishing scale [default=4]')
  parser.add_argument('--coarse', action='store', type=int, default=1,
                      help='sparsity in location [default=1]')
  parser.add_argument('--tics', action='store', type=float, default=0.5,
                      help='tics scale in rotation [default=0.5]')
  parser.add_argument('--range', dest='angle', action='store',
                      type=float, nargs=2, default=(-5.0, 185.0),
                      help='rotation angle range [default=(-5, 185)]')

  parser.add_argument('--rank', action='store', type=int, default=15,
                      help='number of the low-rank components [default=15]')
  parser.add_argument('--sigma', action='store', type=float, default=3.0,
                      help='threshold of soft-threshold [default=3.0]')
  parser.add_argument('--max_iter', action='store', type=int, default=30,
                      help='maximum number of iterations [default=30]')

  parser.add_argument('-v', dest='verbose', action='store_true',
                      help='enable verbose output')
  parser.add_argument('-f', dest='overwrite', action='store_true',
                      help='overwrite the output fits file if exists')

  args = parser.parse_args(argv[1:])

  hdu = fits.open(args.fits)
  image = hdu[0].data
  if image.ndim != 3:
    raise RuntimeError('fits file should contain a 3d-image')

  img = squish_image(image, shrink=args.shrink)
  z,y,x = img.shape

  norm,std = np.mean(img), np.std(img)
  weight = np.median(img,axis=0)
  weight = weight + dilation(weight, disk(3))
  weight /= np.median(weight)

  hough, wmap = hough_transform(
    img/norm/weight, angle_s=args.angle[0], angle_e=args.angle[1],
    tics=args.tics, coarse=args.coarse)
  print('meteor: hough transformation done.', file=sys.stderr)
  n,t,l = hough.shape

  sigmap = safe_divide(hough, np.sqrt(wmap))
  sparse = get_sparse_matrix_by_soft_godec(
    sigmap.reshape((n,t*l)), rank=args.rank,
    sigma=args.sigma, max_iter=args.max_iter)
  print('meteor: sparse matrix extraction done.', file=sys.stderr)
  sigmap = sparse.reshape((n,t,l))

  # hdu.append(hdu[0])
  hdu[0].data = normalize_cube(sigmap)
  # vmedian = np.median(img.reshape((z,x*y)),axis=1).reshape((z,1,1))
  # masked = (img - vmedian) * (weight < 1.01)
  # hdu[1].data = masked

  hdu[0].header.set('CRPIX1', (l+1)/2.0)
  hdu[0].header.set('CRPIX2', 1.0)
  hdu[0].header.set('CUNIT1', 'pixel')
  hdu[0].header.set('CUNIT2', 'deg')
  hdu[0].header.set('CTYPE1', 'NONE')
  hdu[0].header.set('CTYPE2', 'NONE')
  hdu[0].header.set('CRVAL1', 0.0)
  hdu[0].header.set('CRVAL2', args.angle[0])
  hdu[0].header.set('CD1_1', 1.0*args.coarse*args.shrink)
  hdu[0].header.set('CD2_1', 0.0)
  hdu[0].header.set('CD1_2', 0.0)
  hdu[0].header.set('CD2_2', args.tics)
  hdu[0].header.set('DATE', Time.now().isot)

  hdu.writeto(args.output, overwrite=args.overwrite)


if __name__ == '__main__':
  main()
