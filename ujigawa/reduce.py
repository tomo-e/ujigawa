#!/usr/bin/env python
# -*- coding: utf-8 -*-
from argparse import ArgumentParser as ap
import astropy.io.fits as fits
import numpy as np
import os.path as path
from ujigawa.misc import command_line_function

@command_line_function
def main(*argv):
  parser = ap(description='Reduce a image.')

  parser.add_argument('fits', type=str,
                      help='a raw fits image')
  parser.add_argument('dark', type=str, nargs='?',
                      help='a dark frame to reduce the image')
  parser.add_argument('flat', type=str, nargs='?',
                      help='a flat frame to reduce the image')
  parser.add_argument('out', type=str,
                      help='output fits file')
  parser.add_argument('--calib', dest='calib', action='store', default='.',
                      help='path to the directory of calibration data')
  parser.add_argument('--cropped', dest='cropped', action='store_true',
                      help='reduce already cropped fits file')
  parser.add_argument('-f', dest='overwrite', action='store_true',
                      help='overwrite a fits image')

  args = parser.parse_args(argv[1:])

  hdu = fits.open(args.fits)
  if args.cropped is False:
    if hdu[0].data.ndim == 3:
      hdu[0].data = hdu[0].data[:,:,156:]
    elif hdu[0].data.ndim == 2:
      hdu[0].data = hdu[0].data[:,156:]
  hdr = hdu[0].header

  if args.dark is None:
    nx1 = hdr.get('NAXIS1')
    nx2 = hdr.get('NAXIS2')
    qid = hdr.get('QUAD_ID')
    did = hdr.get('DET_ID')
    texp = int(hdr.get('TFRAME')*1000)
    darkfits = '{}/dark_{}x{}_H_{:04d}-{:03d}.fits'.format(
      args.calib, nx1, nx2, texp, did)
  else:
    darkfits = args.dark

  dark_hdu = fits.open(darkfits)
  if dark_hdu[0].data.ndim == 3:
    raise RuntimeError('a wrong fits file assigned as DARK.')
  hdu[0].data = hdu[0].data - dark_hdu[0].data

  if args.flat is None:
    nx1 = hdr.get('NAXIS1')
    nx2 = hdr.get('NAXIS2')
    qid = hdr.get('QUAD_ID')
    did = hdr.get('DET_ID')
    flatfits = '{}/flat_{}x{}_H-{:03d}.fits'.format(
      args.calib, nx1, nx2, did)
  else:
    flatfits = args.flat

  flat_hdu = fits.open(flatfits)
  if flat_hdu[0].data.ndim == 3:
    raise RuntimeError('a wrong fits file assigned as FLAT.')
  hdu[0].data = hdu[0].data / flat_hdu[0].data

  hdu[0].header.add_history(
    '[reduce] created from {}.'.format(path.basename(darkfits)))
  hdu[0].header.add_history(
    '[reduce] created from {}.'.format(path.basename(flatfits)))

  hdu.writeto(args.out, overwrite=args.overwrite)


if __name__ == '__main__':
  main()
