#!/usr/bin/env python
# -*- coding: utf-8 -*-
from argparse import ArgumentParser as ap
import astropy.io.fits as fits
import numpy as np
import os.path as path
import sep
from numba import jit, njit, float32
from ujigawa.misc import command_line_function


def estimate_background(image, sep_option, saturation_limit=25000.):
  mask = image > saturation_limit
  model = sep.Background(image, mask=mask, **sep_option)
  return model.back(np.float32), model.rms(np.float32)


@command_line_function
def main(*argv):
  parser = ap(description='Subtract Background')

  parser.add_argument('fits', help='input fits file')
  parser.add_argument('out', help='output fits file')
  parser.add_argument('--block', type=int, action='store', default=256,
                      help='block size in background estimation')
  parser.add_argument('--filter', type=int, action='store', default=3,
                      help='filter width in background estimation')
  parser.add_argument('--fast', action='store_true',
                      help='fast operation mode (less stable)')
  parser.add_argument('-v','--verbose', action='store_true',
                      help='enable debug messages')
  parser.add_argument('-f', dest='overwrite', action='store_true',
                      help='overwrite a fits image')

  args = parser.parse_args(argv[1:])

  sep_option = dict(
    bw = args.block,  # block width
    bh = args.block,  # block height
    fw = args.filter, # filter width
    fh = args.filter, # filter height
  )

  hdu = fits.open(args.fits)

  if hdu[0].data.dtype.byteorder == '>':
    hdu[0].data = hdu[0].data.byteswap().newbyteorder()
  z,y,x = hdu[0].data.shape
  hdu.append(fits.ImageHDU(name='BG',data=np.zeros((z,y,x))))
  hdu.append(fits.ImageHDU(name='STDDEV',data=np.zeros((z,y,x))))

  from datetime import datetime

  hdu[0].header.add_history(
    '[imdebias] created from {}.'.format(path.basename(args.fits)))

  t0 = datetime.utcnow()
  if args.fast:
    temp = hdu[0].data.reshape(z*y,x)
    bg,rms = estimate_background(temp, sep_option)
    hdu[0].data -= bg.reshape((z,y,x))
    hdu[1].data =  bg.reshape((z,y,x))
    hdu[2].data =  rms.reshape((z,y,x))
    hdu[0].header.add_history(
      '[imdebias] background estimated in FAST mode.')
  else:
    for zz in range(z):
      bg,rms = estimate_background(hdu[0].data[zz], sep_option)
      hdu[0].data[zz] -= bg
      hdu[1].data[zz] = bg
      hdu[2].data[zz] = rms
    hdu[0].header.add_history(
      '[imdebias] background estimated in NORMAL mode.')
  t1 = datetime.utcnow()
  if args.verbose:
    print('elapsed: {}'.format((t1-t0).total_seconds()))


  hdu.writeto(args.out, overwrite=args.overwrite)


if __name__ == '__main__':
  main()
