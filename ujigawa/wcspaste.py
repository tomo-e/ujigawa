#!/usr/bin/env python
# -*- coding: utf-8 -*-
from argparse import ArgumentParser as ap
import astropy.io.fits as fits
import os.path as path
from ujigawa.misc import command_line_function


@command_line_function
def main(*argv):
  parser = ap(description='Copy & Paste WCS-information from a FITS image.')

  parser.add_argument('fits', type=str, help='input fits image')
  parser.add_argument('src', type=str, help='a WCS source image')
  parser.add_argument('out', type=str, help='output fits image')
  parser.add_argument('-f', dest='overwrite', action='store_true',
                      help='overwrite a fits image')

  args = parser.parse_args(argv[1:])

  orig = fits.open(args.fits)
  wcs  = fits.open(args.src)

  orighdr = orig[0].header
  wcshdr  = wcs[0].header

  orighdr['CTYPE1'] = wcshdr['CTYPE1']
  orighdr['CTYPE2'] = wcshdr['CTYPE2']
  orighdr['CRVAL1'] = wcshdr['CRVAL1']
  orighdr['CRPIX1'] = wcshdr['CRPIX1']
  orighdr['CRVAL2'] = wcshdr['CRVAL2']
  orighdr['CRPIX2'] = wcshdr['CRPIX2']
  orighdr['CD1_1'] = wcshdr['CD1_1']
  orighdr['CD1_2'] = wcshdr['CD1_2']
  orighdr['CD2_1'] = wcshdr['CD2_1']
  orighdr['CD2_2'] = wcshdr['CD2_2']
  orighdr['LONPOLE'] = wcshdr.get('LONPOLE', 180)
  orighdr['LATPOLE'] = wcshdr.get('LATPOLE', 0.0)

  if wcshdr['CTYPE1'].find('SIP') > 0:
    orighdr['A_ORDER'] = wcshdr['A_ORDER']
    orighdr['A_0_0'] = wcshdr['A_0_0']
    orighdr['A_0_1'] = wcshdr['A_0_1']
    orighdr['A_0_2'] = wcshdr['A_0_2']
    orighdr['A_1_0'] = wcshdr['A_1_0']
    orighdr['A_1_1'] = wcshdr['A_1_1']
    orighdr['A_2_0'] = wcshdr['A_2_0']
    orighdr['AP_ORDER'] = wcshdr['AP_ORDER']
    orighdr['AP_0_0'] = wcshdr['AP_0_0']
    orighdr['AP_0_1'] = wcshdr['AP_0_1']
    orighdr['AP_0_2'] = wcshdr['AP_0_2']
    orighdr['AP_1_1'] = wcshdr['AP_1_1']
    orighdr['AP_1_0'] = wcshdr['AP_1_0']
    orighdr['AP_2_0'] = wcshdr['AP_2_0']

  if wcshdr['CTYPE2'].find('SIP') > 0:
    orighdr['B_ORDER'] = wcshdr['B_ORDER']
    orighdr['B_0_2'] = wcshdr['B_0_2']
    orighdr['B_1_1'] = wcshdr['B_1_1']
    orighdr['B_2_0'] = wcshdr['B_2_0']
    orighdr['BP_ORDER'] = wcshdr['BP_ORDER']
    orighdr['BP_0_0'] = wcshdr['BP_0_0']
    orighdr['BP_0_1'] = wcshdr['BP_0_1']
    orighdr['BP_0_2'] = wcshdr['BP_0_2']
    orighdr['BP_1_1'] = wcshdr['BP_1_1']
    orighdr['BP_1_0'] = wcshdr['BP_1_0']
    orighdr['BP_2_0'] = wcshdr['BP_2_0']

  orighdr.add_history(
    '[wcspaste] created from {}'.format(path.basename(args.fits)))
  orighdr.add_history(
    '[wcspaste] wcs source file: {}'.format(path.basename(args.src)))

  orig.writeto(args.out, overwrite=args.overwrite)


if __name__ == '__main__':
  main()
