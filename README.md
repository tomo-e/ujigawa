# Ujigawa: Data Analysis tools for the Tomo-e Gozen project
[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)

## Install
```sh
pip install git+https://bitbucket.org/tomo-e/ujigawa/src/master/
```

Use `pip uninstall ujigawa` to remove this package.


## Description
This package provides some useful commands to reduce 3D-FITS data obtained with the Tomo-e Gozen camera[^tomoeweb]. Tomo-e Gozen is a mosaic CMOS camera developed for the 1.05-m Kiso Schmidt Telescope. A prototype model[^sako2016] had bee developed in 2016, and the final model with 84 CMOS sensors[^sako2018] has been completed in November, 2019. How the Tomo-e Gozen's data are processed is briefly described in Ohsawa _et al._ (2016).[^ohsawa2016] The following commands, written in Python 3, are available:

#### Image Statistics
- imstat: calculate statistics of the image.
- imstat3: calculate statistics of the image along with `NAXIS3`.
- listhead: create a list of FITS Keywords for multiple FITS files.

#### Image Manipulation
- imarith: arithmetic operations of the two images.
- imdebias: subtract background biases from the image..
- imslant: make the image slanted by the give angle.
- imzdiff: make a differential image along with `NAXIS3`.
- shiftadd: make a shift-and-add image along with `NAXIS3`.
- subimage: extract a subset of the image.
- wcspaste: override the WCS information using that of the source FITS file.

#### Basic Calibration
- makedark: make a dark frame from a set of images.
- makeflat: make a flat frame from a set of images and a dark frame.
- nakamura: subtract a background using the Nakamura method.
- reduce: reduce a FITS file using dark and flat frames.

#### Photometry
- extract3: extract a specified source along with `NAXIS3`.
- phot3: extract a specified source from a 3D FITS file.

### Meteors
- hough: make a Hough-transformed FITS file.
- streakfind: find a streak from a Hough-transformed FITS file.

#### Obsolete Commands
- cropimage: extract a photo-sensitive area.
- debias: subtract a bias pattern estimated from the reference pixels.
- meteor: search streaks in a 3D-FITS image.


### Dependencies
TBW...


[tomoe]: http://www.ioa.s.u-tokyo.ac.jp/tomoe/


[^tomoeweb]: _see,_ [The Tomo-e Gozen Project Web Page][tomoe]
[^sako2016]: Sako, S., _et al._, 2016, in: Proc. SPIE., pp. 99083P–15. [https://doi.org/10.1117/12.2231259](https://doi.org/10.1117/12.2231259)
[^ohsawa2016]: Ohsawa, R., _et al._, 2016, in: Proc. SPIE., pp. 991339–8. [https://doi.org/10.1117/12.2231615](https://doi.org/10.1117/12.2231615)
[^sako2018]: Sako, S., _et al._, 2018, in: Proc. SPIE., p. 107020J. [https://doi.org/10.1117/12.2310049](https://doi.org/10.1117/12.2310049)
