#!/usr/bin/env python
# -*- coding: utf-8 -*-
from glob import glob
from setuptools import setup, find_packages
import os,sys,re


with open('README.md', 'r') as fd:
  version = '0.3'
  author = 'Ryou Ohsawa'
  email = 'ohsawa@ioa.s.u-tokyo.ac.jp'
  description = ''
  long_description = fd.read()
  license = 'MIT'

classifiers = [
  'Development Status :: 3 - Alpha',
  'Environment :: Console',
  'Intended Audience :: Science/Research',
  'License :: OSI Approved :: MIT License',
  'Operating System :: POSIX :: Linux',
  'Programming Language :: Python :: 3',
  'Programming Language :: Python :: 3.7',
  'Topic :: Scientific/Engineering :: Astronomy']

entry_points = {
  'console_scripts': [
    'imstat = ujigawa.imstat:main',
    'imstat3 = ujigawa.imstat3:main',
    'listhead = ujigawa.listhead:main',
    'imarith = ujigawa.imarith:main',
    'imdebias = ujigawa.imdebias:main',
    'imslant = ujigawa.imslant:main',
    'imzdiff = ujigawa.imzdiff:main',
    'shiftadd = ujigawa.shiftadd:main',
    'subimage = ujigawa.subimage:main',
    'wcspaste = ujigawa.wcspaste:main',
    'makedark = ujigawa.makedark:main',
    'makeflat = ujigawa.makeflat:main',
    'nakamura = ujigawa.nakamura:main',
    'reduce = ujigawa.reduce:main',
    'extract3 = ujigawa.extract3:main',
    'phot3 = ujigawa.phot3:main',
    'hough = ujigawa.hough:main',
    'streakfind = ujigawa.streakfind:main',
    'debias = ujigawa.debias:main',
  ]
}


if __name__ == '__main__':
  try:
    import numpy
  except ImportError:
    raise SystemExit('NumPy is not available.')

  setup(
    name='ujigawa',
    version=version,
    author=author,
    author_email=email,
    maintainer=author,
    maintainer_email=email,
    description=description,
    long_description=long_description,
    long_description_content_type='text/markdown',
    url='https://bitbucket.org/tomo-e/ujigawa/src/master/',
    license=license,
    packages=find_packages(),
    classifiers=classifiers,
    entry_points=entry_points,
    install_requires=[
      'astropy','scipy','sep','scikit-learn','scikit-image'])
